GRU Devlog 19 - gic and several libs

This week started with making gic (maybe name will be change). As I have written before
it is going to be suckless ii clone. Also I started making girclib - simple C library for irc 
protocol. I think it will be published as separate project, because I would like to make it
for both: client and server. I don't yet how it will be done but we will see...

Then I thought that would be cool to have own libc, but have no knowledge how it speaks with OS.
Then I understood that on linux it is working via syscalls. So started making liblinux -
C library for linux syscalls. Using it we won't need to use assembly a lot in our libc. Currently
write and read syscalls are done and programmer can use it if passed -I, -L and -l flags to compiler.
And it is already published! (check codeberg or tildegit)

After understanding how to create proper makefile with liblinux, I started making olibc. Currently
it has very tiny number of features. But string.h is mostly done! I don't know when I will fill that
it will be ready enough to be published, but hope to have useful functions and test suit there till
that time.

When olibc will be done enough I am going to start making C compiler (already have some code, but it
is old and I not understand how it works, so need to be rewritten). Also I hope that we will have 
enough toolchain (or I will have enough knowledge to implement things that are not done faster) 
for our own Linux or BSD distribution. But that will be in future...

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, gic, girclib, liblinux, olibc, libs
