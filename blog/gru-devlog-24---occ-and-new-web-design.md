GRU Devlog 24 - occ and new web design

This week I was working on occ

First I have done lexer. Seems it is totally working, if we will need to add some new keywords, 
just adding if statement is needed. But maybe will be better to rewrite it to linked list,
but I didn't understood how it works so for now just list of tokens.

First thing I have done after lexer is simple code generator. So occ can generate very simple
assembler code! For now it can just generate functions (int) and return (just number). But I
think it was cool progress in 5 days.

Then I started making parser. It took several days to properly implement binary trees, but 
seems it is done now. It can make very simple tree with function token and return that can
be understood by code generator function

Also chunk has made very cool website design and logo. If you read this devlog, you can see 
how cool is it :)

Hope you liked this post! If you would like to help us, contact me via email, xmpp or irc :)

tags: gru, occ
