GRU Devlog 5 - First contribution from other person, coreutils and gxt (tui text editor)

This week I had a lot of new ideas. But I think we have to work on something that 
have already been started. 
Also we now have one more coreutils rewrite
(now on C to more easily port it to Orion in future). 

Also i have started working on some 
projects that is not ready to be published yet. One of them is gxt - tui text editor.

Currently it can only move cursor and display files. But I think I'll publish it next week 
when editing will be working.

Bad news is that my school lessons will start next week so maybe this devlogs won't be
published every week, because of not enough work done. But I don't know, maybe I'll have 
enough time.

Also good news - we have first contribution on codeberg to grsh. I didn't know this person 
(because he is not from tildeverse :) ), so it is cool!

Hope you liked this devlog! If you can somehow help us, please contribute to our projects 
on tildegit/codeberg and/or message me in someway :)

tags: gru, coreutils, contribution, gxt, grsh
