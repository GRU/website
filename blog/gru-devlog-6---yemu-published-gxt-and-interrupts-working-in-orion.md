GRU Devlog 6 - yemu, published gxt and interrupts working in Orion

This week I have published gxt, but it currently works as more (only down scrolling).
Also it has a lot of of runtime error (Segmentation fault and others).

I have started working on yemu - Yet another EMUlator. It currently supports only 6502
proccessor with tiny number of instructions (LDA, LDX and LDY)

Today we have fixed interrupts in Orion and now I can implement timer, paging, keyboard 
and a lot of other things (thanks to quinn and smlckz from tilde.chat)

Hope you liked this post and if you would like to help me, 
contact me via email, xmpp or irc :)

tags: gru, gxt, yemu, orion
