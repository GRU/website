GRU Devlog 8 - orsh and published initrd

This week I have published initrd, but it is not working as expected.

Also I have started one more project - orsh. It is one more shell, but now in C. 
Maybe it will be easier to port to Orion. It already supports ; but they are working a bit weird.

Also in orsh you can work with environment variables. 
From today's morning I am trying to replace bash with it, but still a lot need to be done.

Hope you liked this post and if you would like to help me, contact me via email, xmpp or irc :)

tags: gru, orsh, orion
